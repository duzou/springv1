package com.zenghus.demo.controller;

import com.zenghus.demo.annotation.Autowired;
import com.zenghus.demo.annotation.Controller;
import com.zenghus.demo.annotation.RequestMapping;
import com.zenghus.demo.annotation.RequestParam;
import com.zenghus.demo.service.DemoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author zenghu
 * @date 2018/4/28 18:52
 */
@Controller
@RequestMapping("/demo")
public class DemoController {
    @Autowired
    DemoService demoService;

    @RequestMapping("/query.json")
    public void query(@RequestParam("name") String name, HttpServletRequest reqeust, HttpServletResponse response){
        String result=demoService.get(name);
        try {
            response.getWriter().write(result);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
